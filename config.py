import os
import secret

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or secret.secretnumber
    UPLOAD_FOLDER = ''
    MAX_CONTENT_PATH = 1000000
    ALLOWED_EXTENSIONS = set(['docx'])