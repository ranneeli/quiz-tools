from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField,TextAreaField, BooleanField, SubmitField
from wtforms.validators import DataRequired

class RawText(FlaskForm):
    inputstr = TextAreaField('Paste your text here')
    submit = SubmitField('Upload')

class KSend(FlaskForm):
    curlstring = TextAreaField('Paste your curl string here')
    jsonstring = TextAreaField('Paste your json ourput here')
    submit = SubmitField('Upload')


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')