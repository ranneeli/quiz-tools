from flask import Flask, Response, request, render_template, flash, redirect, send_file
from config import Config
from werkzeug.utils import secure_filename
import os
import input_form
from quizscripts import quiz_writer
from quizscripts import kahoot_send

app = Flask(__name__)
app.config.from_object(Config)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']


@app.route('/')
def home():
    return render_template('home.html')

@app.route("/uploadraw", methods=['GET', 'POST'])
def uploadraw():
    form = input_form.RawText()
    if form.validate_on_submit():
        return redirect('/dbgpage')
    return render_template('uploadraw.html', form=form)

@app.route("/uploadfile", methods=['GET', 'POST'])
def uploadfile():
    form = input_form.RawText()
    if form.validate_on_submit():
        return redirect('/dbgpage')
    return render_template('uploadfile.html', form=form)

@app.route('/uploader', methods = ['GET', 'POST'])
def uploader():
    if request.method == 'POST':
        f = request.files['file']
        if f and allowed_file(f.filename):
            filename = secure_filename(f.filename)
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            flash('File uploaded successfully')
            q_path, k_path, log_path = quiz_writer.qwriter(filepath=filename, type=0, q_out=1, k_out=1)
            flash('File converted successfully')
            #send_file('tmp/'+q_path, as_attachment=True, attachment_filename='quizizz.xlsx')
            #send_file('tmp/'+k_path, as_attachment=True, attachment_filename='kahoot_json.txt')
            #send_file('tmp/'+log_path, as_attachment=True, attachment_filename='log_path.txt')
            dloads = ['Quizizz', 'Kahoot', 'Log']
            dloads_src = ['/dl/'+q_path , '/dl/'+k_path, '/dl/'+log_path]
            return render_template('uploadsuccess.html', dloads=dloads, dloads_src=dloads_src)
        else:
            flash('Invalid file name or extension')
            return render_template('uploadfile.html')
    else:
        flash('No file detected')
        return render_template('uploadfile.html')

@app.route('/downloads')
def downloads():
    return render_template('downloads.html')

@app.route('/kahootsend', methods = ['GET', 'POST'])
def kahootsend():
    form = input_form.KSend()
    if form.validate_on_submit():
        curlstring=request.form['curlstring']
        jsonstring=request.form['jsonstring']
        status_code = kahoot_send.ksend(curlstring, jsonstring)
        flash('Status code: ' + status_code)
        return render_template('kahootsend.html', form=form)

    return render_template('kahootsend.html', form=form)

@app.route('/dbgpage')
def dbgpage():
    return '''<html><body><p>debug page</p><li>{{ get_flashed_messages() }}</li></body></html>'''

@app.route("/getPlotCSV")
def getPlotCSV():
    csv = '1,2,3\n4,5,6\n'
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=myplot.csv"})

@app.route("/getWordTemplate")
def getWordTemplate():
    return send_file('static/templatedocs/bulk_quiz_word_template.docx', as_attachment=True, attachment_filename='wordtemplate_placeholder.docx')


@app.route("/dl/<file_name>", methods=['GET', 'POST'])
def getFile(file_name):
    return send_file('tmp/'+file_name, as_attachment=True)