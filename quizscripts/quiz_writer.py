from docx import Document
import xlsxwriter
import json
import datetime


# links need to be added manually
# then need to use dev tools to get the curl statement, then send it through bash with --binary-data '@path'

def kahoot_header(title_str='default title', description_str='default description'):
	json1_str = r'{"audience":"University","resources":"","created":null,"introVideo":"","metadata":{"resolution":null,"moderation":{"flaggedTimestamp":0,"timestampResolution":0}},"visibility":0,"title":"Test","language":"English","organisation":null,"uuid":null,"description":"test","themeId":null,"questions":[{"points":true,"resources":"","time":20000,"choices":[{"answer":"a1 correct","correct":true},{"answer":"a2","correct":false},{"answer":"a3","correct":false}],"type":"quiz","video":{"fullUrl":"","service":"youtube","startTime":0,"endTime":0,"id":""},"id":"1","image":"","questionFormat":0,"question":"q1","numberOfAnswers":3},{"points":true,"resources":"","time":20000,"choices":[{"answer":"a1","correct":false},{"answer":"a2","correct":false},{"answer":"a3 corr","correct":true},{"answer":"a4","correct":false}],"type":"quiz","video":{"fullUrl":"","service":"youtube","startTime":0,"endTime":0,"id":""},"id":"2","image":"","questionFormat":0,"question":"q2","numberOfAnswers":4}],"quizType":"quiz","type":"quiz"}'
	k_default = json.loads(json1_str)

	# delete questions
	#print(type(k_default['questions'][0]))
	k_default['questions'] = []

	# set title and description
	k_default['title'] = title_str
	k_default['description'] = description_str

	return k_default

def kahoot_qformat(q_list):
	q_str = r'{"points":true,"resources":"","time":20000,"choices":[{"answer":"a1","correct":false},{"answer":"a2","correct":false},{"answer":"a3 corr","correct":true},{"answer":"a4","correct":false}],"type":"quiz","video":{"fullUrl":"","service":"youtube","startTime":0,"endTime":0,"id":""},"id":"2","image":"","questionFormat":0,"question":"q2","numberOfAnswers":4}'
	q_def = json.loads(q_str)
	q_def['choices'] = [] # clear choices list

	return q_def

def qz_headings(worksheet):
	# Writes the headings

	worksheet.write('A1', 'Question')
	worksheet.write('B1', 'Option 1')
	worksheet.write('C1', 'Option 2')
	worksheet.write('D1', 'Option 3')
	worksheet.write('E1', 'Option 4')
	worksheet.write('F1', 'Option 5')
	worksheet.write('G1', 'Correct Answer')
	worksheet.write('H1', 'Time')
	worksheet.write('I1', 'Image Link')

	worksheet.write('A2', 'Text of the question (required)')
	worksheet.write('B2', 'Text for option 1 (required)')
	worksheet.write('C2', 'Text for option 2 (required)')
	worksheet.write('D2', 'Text for option 3 (optional)')
	worksheet.write('E2', 'Text for option 4 (optional)')
	worksheet.write('F2', 'Text for option 5 (optional)')
	worksheet.write('G2', 'Integer (1-5 for the correct option)')
	worksheet.write('H2', 'Time in seconds (optional, default value is 30)')
	worksheet.write('I2', 'Link of the image (optional)')

def get_all_questions(document, ifk, ifq, worksheet=None):

	q_dict = []; # this will be a list of dicts

	q_num = 0 # question number
	n_paras = len(document.paragraphs) # number of paragraphs
	q_list = []

	if ifk:
		# default question template for kahoot only
		q_def = kahoot_qformat(q_list)

	# find all question indicies
	for p_num in range(0, n_paras):
		# detect if there is a question
		para = document.paragraphs[p_num]
		#print(para.text + para.style.name)

		if para.style.name=='Question':
			q_num = q_num+1 # increment question number
			q_list.append(p_num)
			#worksheet.write('A'+str(q_num+2), para.text) # write question text

	# append a number for last "question"
	q_list.append(n_paras)

	# list of questions with multiple answers, skipped
	multians = []
	k_skipped = []
	q_skipped = []
	imglist = []
	skip_len = []
	skip_noans = []
	skip_nopts = []

	# go through questions
	for k in range(0, len(q_list)-1):
		q_ind = q_list[k] # index of question

		# check the validity of questions and answer lengths and if there's a correct answer marked before we add it
		found_right = 0 # was a right answer found?
		n_opts = q_list[k+1]-q_ind-1 # find number of options
		k_valid = 1 # validity of the question for kahoot
		q_valid = 1 # validity of the question for quizizz

		v_opts = n_opts

		# question length
		if len(document.paragraphs[q_ind].text) > 95:
			k_valid = 0
			skip_len.append(k+1)
		for a in range(0, n_opts):
			# answer length
			if document.paragraphs[q_ind+a+1].style.name=='Right answer':
				if len(document.paragraphs[q_ind+a+1].text) > 60:
					k_valid = 0
					skip_len.append(k+1)
				# check if already got an answer
				if found_right:
					# multiple correct answers marked
					print('>1 answers found for question ' + str(k+1))
					multians.append(k+1)
				else:
					found_right = 1

			elif document.paragraphs[q_ind+a+1].style.name=='Wrong answer':
				if len(document.paragraphs[q_ind+a+1].text) > 60:
					k_valid = 0
					skip_len.append(k+1)
			elif document.paragraphs[q_ind+a+1].style.name=='Image link':
				v_opts = v_opts - 1 # number of valid options

		# right answer found?
		if found_right==0:
			print('No answer found for question ' + str(k+1))
			k_valid = 0
			q_valid = 0
			skip_noans.append(k+1)

		# number of options
		if v_opts > 4:
			k_valid = 0
			skip_nopts = skip_nopts.append(k+1)
		if v_opts > 5:
			q_valid = 0
			skip_nopts = skip_nopts.append(k+1)

		if k_valid == 0:
			k_skipped.append(k+1)

		if q_valid == 0:
			q_skipped.append(k+1)

		if k_valid == 0 and q_valid == 0:
			# neither question was valid, so can skip processing
			continue

		nq_skipped = len(k_skipped)
		k_c = k-nq_skipped

		if ifk:
			# add kahoot template

			q_dict.append(q_def.copy()) # append default question
			q_dict[k_c]['question']=document.paragraphs[q_ind].text # write question text
			q_dict[k_c]['id'] = str(k_c+1) # question ID
			q_dict[k_c]['numberOfAnswers'] = n_opts # write number of answers
			c_choices = [] # empty choices

		if ifq:
		    # write question text
		    worksheet.write('A'+str(k_c+3), document.paragraphs[q_ind].text)

		# write answers
		for a in range(0, n_opts):
			if document.paragraphs[q_ind+a+1].style.name=='Right answer': # write correct option
				if ifk and k_valid:
					c_choices.append({'answer':document.paragraphs[q_ind+a+1].text,'correct':True})
				if ifq and q_valid:
					worksheet.write(k_c+2, a+1, document.paragraphs[q_ind+a+1].text)
					worksheet.write(k_c+2, 6, a+1)

			elif document.paragraphs[q_ind+a+1].style.name=='Wrong answer': # write other answers
				if ifk and k_valid:
					c_choices.append({'answer':document.paragraphs[q_ind+a+1].text,'correct':False})
				if ifq and q_valid:
					worksheet.write(k_c+2, a+1, document.paragraphs[q_ind+a+1].text)

			elif document.paragraphs[q_ind+a+1].style.name=='Image link': # write image link
				imglist.append(k_c+1)
				if ifk and k_valid:
					print('Image link found for question ' + str(k+1))
				if ifq and q_valid:
					worksheet.write(k_c+2, 8, document.paragraphs[q_ind+a+1].text)

				#print(document.paragraphs[q_ind+a+1].text)
			#else:
				#print('Found unidentified line for question ' + str(k+1))


		if ifk:
			q_dict[k_c]['choices'] = c_choices

	print('Found ' + str(len(q_list)-1) + ' questions')
	print('Skipped ' + str(nq_skipped) + ' questions')

	log_dict = {'Questions found':len(q_list)-1, 'Quizizz skipped question number(s)':q_skipped, 'Kahoot skipped question number(s)':k_skipped, 'Image link found for question number(s)':imglist, 'Multiple answers found for question number(s)':multians, 'Skip reason - length':skip_len, 'Skip reason - no answer':skip_noans, 'Skip reason - too many choices':skip_nopts}

	return q_dict, log_dict



def qwriter(filepath, type, q_out, k_out):

	d = datetime.datetime.now()
	cDate = '{:%Y-%m-%d-%H-%M-%S}'.format(d)

	k_path = cDate + '_kahootjson.txt'
	q_path = cDate + '_quizizz.xlsx'
	log_path = cDate + '_log.txt'

	# get file (word)
	document = Document(filepath)

	# open worksheet
	if q_out:
		workbook = xlsxwriter.Workbook(q_path)
		worksheet = workbook.add_worksheet()
		# Create headings
		qz_headings(worksheet)
	else:
		worksheet = []

	# create dict with all questions
	q_dict, log_dict = get_all_questions(document, k_out, q_out, worksheet)

	# if kahoot output required
	if k_out:
		title_str= 'Bulk Upload Quiz'
		description_str='Bulk Upload Quiz default description'
		k_dict = kahoot_header(title_str=title_str, description_str=description_str)
		# add questions
		k_dict['questions'] = q_dict
		with open(k_path, 'w') as outfile:
			json.dump(k_dict, outfile)


	if q_out:
		# Close the Excel file
		workbook.close()

	with open(log_path, 'w') as outfile:
			json.dump(log_dict, outfile)


	return q_path, k_path, log_path
