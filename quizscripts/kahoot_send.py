import requests

def ksend(curlstring, jsonstring):

    url = 'https://create.kahoot.it/rest/kahoots'

    splitstring = curlstring.split(' -H ')

    headers = dict()

    for k in range(1, len(splitstring)-1):
    	cstr = splitstring[k].split(':', 1)
    	headers[cstr[0][1:]] = cstr[1][1:-1]

    headers['Connection'] = 'keep-alive'

    r = requests.post(url, data=jsonstring.encode('utf-8'), headers=headers)
    #r = requests.post('http://httpbin.org/post', data = {'key':'value'})


    return r.status_code